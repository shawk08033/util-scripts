<?php

/**
 * This script is used to read equipment dat from a spreadsheet and insert it in a database table
 */

ini_set('error_reporting', E_ALL);
ini_set('display_errors', true);
ini_set('memory_limit', '5024M');
ini_set('max_execution_time', 2700);

require_once 'vendor/simplexlsx/SimpleXLSX.php';

$connection = mysqli_connect("127.0.0.1", "root", "pass", "customer");
if(mysqli_connect_errno()) {
  die("Database connection failed: " . 
       mysqli_connect_error() . 
       " (" . mysqli_connect_errno() . ")"
  );
}
else {
  $site= readline("Enter site id: ");

  $dir    = '/Users/shaun/equips/';
  $file = scandir($dir);
  $file = array_diff($file, [".", "..",".DS_Store"]);
  $filename = array_pop($file);

  if ( $xlsx = SimpleXLSX::parse($dir . $filename) ) {
   $rows = $xlsx->rows();

   for($i=11; $i<count($rows); $i++)
   {
        $array = [];

        $array["tag"] = "'" . escapeString($rows[$i][0]) . "'";
        $array["SystemType"] = "'" . escapeString($rows[$i][1]) . "'";
        $array["EquipmentType"] = "'" . escapeString($rows[$i][2]) . "'";
        $array["AreaServed"] = "'" . escapeString($rows[$i][3]) . "'";
        $array["YearInstalled"] = "'" . escapeString($rows[$i][4]) . "'";
        $array["Manufacturer"] = "'" . escapeString($rows[$i][5]) . "'";
        $array["Model"] = "'" . escapeString($rows[$i][6]) . "'";
        $array["Serial"] = "'" . escapeString($rows[$i][7]) . "'";
        $array["Characteristics"] = "'" . escapeString($rows[$i][8]) . "'"; 

        $sqlVals = "(" . $site . "," . implode(',', array_values($array));

        $query = "INSERT INTO `customer`.`equipment`(`siteId`,`EquipmentTagID`,`SystemType`,`EquipmentType`,`AreaServed`,`YearInstalled`,`Manufacturer`,`Model`,`Serial`,`Characteristics`)" .
                  "VALUES " . $sqlVals . ");";

        $data = mysqli_query($connection, $query);
	      echo $query;

    }
  } else {
    echo SimpleXLSX::parseError();
  }
echo "Done";
}

function escapeString($string)
{
  $connection = mysqli_connect("127.0.0.1", "root", "pass", "customer");
  if(mysqli_connect_errno()) {
    die("Database connection failed: " . 
         mysqli_connect_error() . 
         " (" . mysqli_connect_errno() . ")"
    );
  }
   return mysqli_escape_string($connection, $string);
}
?>
