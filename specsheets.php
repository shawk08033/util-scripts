<?php

/**
 * This script is used to read urls of file from a spreadsheet and download them
 */

ini_set('error_reporting', E_ALL);
ini_set('display_errors', true);
ini_set('memory_limit', '5024M');
ini_set('max_execution_time', 2700);

require_once 'vendor/simplexlsx/SimpleXLSX.php';

  $dir    = '/Users/shaunhawk/specsheets/';
  $file = scandir($dir);
  $file = array_diff($file, [".", "..",".DS_Store"]);
  $filename = array_pop($file);

  echo $filename;

  if ( $xlsx = SimpleXLSX::parse($dir . $filename) ) {
   foreach ($xlsx->rows() as $key => $arr) {

      if($key != 0) {
        $locationName = $arr[0];

        echo "wget " . $locationName;

        exec("wget " . $locationName);
      }
    }
  } else {
    echo SimpleXLSX::parseError();
  }


echo "Done";

?>
